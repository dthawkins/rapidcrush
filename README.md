# Rapid Crush
## Form Test
### Installation
1. Move the sourec into a folder that is accesible to the WWW.
2. Create an `uploads` directory for the avatar images.
3. Simply browse to the form at http://<DOMAIN>/form.php

### Form Usage
* Simply send the URL to the form to user to complete
* The form data will be captured in a database and a copy will be sent to the sales team.
* NOTE: All fields are required

### TODO
1. Move DB connection to a seperate file that would be included. - DONE
2. Add avatar upload - DONE
3. Send an email to the sales team with this form data - DONE

### Possible Improvements
1. Optimize the imasges after they have been uploaded; resize and compress
2. Improve user input sanatation and validation
3. Build a form helper that would allow for easy addion of data elements in the future
4. Add a re-captcha for bot prevenetion
