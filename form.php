<?php

error_reporting(-1);
ini_set( 'display_errors', 'On');

date_default_timezone_set('America/New_York');

define('SALES_EMAIL', 'dthawkins@gmail.com');
define('UPLOAD_DIR', 'uploads/');

require_once __DIR__ . "/inc/db.inc";

$formSubmitted = false;
$formSubmitReady = false;
$formErrors = array();
$formErrorHTML = '';

// pre processing checks
if ( !empty($_REQUEST['formSubmit']) ) {
    $formSubmitted = true;
}

if ( $formSubmitted ) {

    // validation
    $formValid = false;
    $nameFirst = trim($_REQUEST['nameFirst']);
    if ( emptY($nameFirst) ) {
        $formErrors[] = 'Invalid first name';
    }
    $nameLast = trim($_REQUEST['nameLast']);
    if ( emptY($nameLast) ) {
        $formErrors[] = 'Invalid last name';
    }
    $emailAddress = trim($_REQUEST['emailAddress']);
    if ( emptY($emailAddress) ) {
        $formErrors[] = 'Invalid email address';
    }
    $salesOption = (int)$_REQUEST['salesOption'];
    if ( emptY($salesOption) ) {
        $formErrors[] = 'Invalid sales option';
    }

    $timeAdded = time();
    $avatarFileName = '';

    if ( empty($formErrors) ) {
        $formValid = true;
    }

    // check for an avatar img
    $avatarUploaded = false;
    if ( !empty($_FILES['avatar']) ) {
        $avatarFileName = basename($_FILES['avatar']['name']);
        $image = getimagesize($_FILES['avatar']['tmp_name']);
        if ( $image !== false && !($image[0] <= 0 && $image[1] <= 0) ) {
            if ( move_uploaded_file($_FILES['avatar']['tmp_name'], UPLOAD_DIR . $avatarFileName) ) {
                $avatarUploaded = true;
            } else {
                $formErrors[] = 'Failed uploading the avatar.';
            }
        } else {
            $formErrors[] = 'The avatar does not appear to be a valid image.';
        }
    }

    if  ( $formValid && $avatarUploaded ) {
        $formSubmitReady = true;
    }

    if ( $formSubmitReady ) {
        $sql = "INSERT INTO rapidcrushForm SET " .
        "nameFirst = '" . $nameFirst . "', " .
        "nameLast = '" . $nameLast . "', " .
        "emailAddress = '" . $emailAddress . "', " .
        "salesOption = '" . $salesOption ."', " .
        "avatarFileName = '" . $avatarFileName ."', " .
        "timeAdded = '" . $timeAdded . "'";

        if ( !$db->query($sql) ) {
            $formSubmitted = false;
            $formErrors[] = 'Opps. Something went wrong. Please try again';
        } else {
            // send email to salesOption
            $subject = "Form Submission from " . $nameFirst . " " . $nameLast;

            $message = "Form Submission\r\n";
            $message .= "Submitted: " . date('Y-m-d H:i', $timeAdded) . "\r\n";
            $message .= "First Name: " . $nameFirst . "\r\n";
            $message .= "Last Name: " . $nameLast . "\r\n";
            $message .= "Last Name: " . $nameLast . "\r\n";
            $message .= "Sales Option: " . $salesOption . "\r\n";
            $message .= "Avatar: " . $avatarFileName . "\r\n";
            mail(SALES_EMAIL, $subject, $message);
        }
    }
}

if ( !empty($formErrors) ) {
    $formErrorHTML = "<p class='bg-danger'>" . implode('<br>', $formErrors) . "</p>";
    $formSubmitted = false;
}
if ( $formSubmitted ) {
    header('Location: ./thankyou.php');
    exit(0);
} else {
echo <<<HTML
<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Bootstrap -->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" type="text/css">

<!--[if lt IE 9]><script type="text/javascript" src="http://getbootstrap.com/docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script type="text/javascript" src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script type="text/javascript" src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
<!-- jQuery -->
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
</head>
<body>
<div class="container">
    <h2>Please complete this form</h3>
    {$formErrorHTML}
    <form action="form.php" method="post" role="form" enctype="multipart/form-data">
        <div class="form-group">
            <label for="nameFirst">First Name</label>
            <input tabindex="1"
                type="text"
                class="form-control"
                name="nameFirst"
                id="nameFirst"
                placeholder="Frist Name"
                required>
        </div>
        <div class="form-group">
            <label for="nameLast">Last Name</label>
            <input tabindex="2"
                type="text"
                class="form-control"
                name="nameLast"
                id="nameLast"
                placeholder="Last Name"
                required>
        </div>
        <div class="form-group">
            <label for="emailAddress">Email Address</label>
            <input tabindex="3"
                type="email"
                class="form-control"
                name="emailAddress"
                id="emailAddress"
                placeholder="Email Address"
                required>
        </div>
        <div class="form-group">
            <label  for="salesOption">Sales Option</label>
            <select tabindex="4" name="salesOption" id="salesOption" class="form-control" required>
                <option value="0">Please Select an Option</option>
                <option value="1">Option 1</option>
                <option value="2">Option 2</option>
                <option value="3">Option 3</option>
            </select>
        </div>
        <div class="form-group">
            <label for="avatar">Avatar</label>
            <input type="file" id="avatar" name="avatar" required>
        </div>
        <button tabindex="5"
            class="btn btn-lg btn-primary btn-block"
            type="submit"
            id="formSubmit"
            name="formSubmit"
            value="Save"
         style="margin-top: 15px;">Save</button>
    </form>
</div> <!-- /container -->

HTML;

}

echo <<<HTML
</body>
</html>

HTML;

exit(0);
